(function(DSH_PAGE) {
	'use strict';

	window.TemplateManager = function(tmplElement, templateScope){

		var services = {
				dcyUtilsFactory 				: invokeAngularService('dcyUtilsFactory'),
				dcyEnvProvider					: invokeAngularService('dcyEnvProvider'),
				dcyConstants					: invokeAngularService('dcyConstants'),
				dcyRenderingEnviromentFactory	: invokeAngularService('dcyRenderingEnviromentFactory')
			},
			_self = this;

		if(!angular || !window.isElement(tmplElement) || !window.isValidObject(templateScope)){
			services.dcyUtilsFactory.errorToConsole('Parameters is not valid for Template instantiate', 'TemplateManager');
			return null;
		}

		var getInternalTmplPath = function (templateName) {
	    	return services.dcyEnvProvider.getItem('rootPath') + services.dcyConstants.TEMPLATES_PATH + templateName + services.dcyConstants.VIEWS_CONVENTION_REF + '.html';
		};

		var ABSOLUTE_URI = services.dcyEnvProvider.getItem('domainUrl') + services.dcyEnvProvider.getItem('contextPath');

		_self.statusEnum = {
				STARTED 	: '0',
				ERROR 		: '1',
				SUCCESS 	: '2',
				COMPLETE	: '3'
		};

		var _infos = {
				element 			: tmplElement,
				status 				: _self.statusEnum.STARTED,
				loadingTemplateName : 'tmplMgrLoading',
				errorTemplateName 	: 'tmplMgrError',
				templatePath 		: '',
				templateName 		: ''
		};

		_self.getResourcePath = function(){
			return _infos.details.resourcePath;
		};

		_self.getLogicalName = function(){
			return _infos.details.logicalName;
		};

		_self.setApi = function(){

			var decisyon = templateScope.DECISYON;
			var content = _self.getDataCtx();

			if(typeof decisyon === 'undefined'){
				decisyon = jQuery.extend({}, window.DECISYON);

				decisyon.target = new Object();

				if(DSH_PAGE){
					var objid = _infos.element.getAttribute('objid'),
						dshElement = DSH_PAGE.getDshElement(objid);

					angular.extend(decisyon.target, dshElement.api(), {	page : DSH_PAGE.getDshElement(DSH_PAGE.getDshID()).api() });
				}

				decisyon.target.content = {
						helper : {
							getCell : function(fieldName, indexRow){
								var data 		= _self.getDataCtx().data;

								if(!data || !data.colMapReportProvider){
									errorToConsole('Data or colMap not present for this widget.');
									return;
								}

								var customField = data.colMapReportProvider.colMap[fieldName];

								if(!customField){
									errorToConsole('Custom fields not defined for this widget.');
									return;
								}

								var reportNode 	= customField.reportNode;
								var report 		= data[reportNode];
								var cellIdx 	= report.colMap[fieldName].cellsIdx;

								return report.rows[indexRow] ? report.rows[indexRow].cells[cellIdx] : undefined;
							}
						}
				};

				decisyon.template =  {
					libraryPath : services.dcyUtilsFactory.getLibraryPath
				};
			}
			//it is an update
			else{
				var refObjType = services.dcyUtilsFactory.getCtxValue(content.ctx, 'refObjType').value;
				if(refObjType == '280'){ //DATA_CONNECTOR
					var refObjId 	= services.dcyUtilsFactory.getCtxValue(content.ctx, 'refObjId').value;
					var owner 		= services.dcyUtilsFactory.getCtxValue(content.ctx, 'owner').value;
					var mshPageId 	= services.dcyUtilsFactory.getCtxValue(content.ctx, 'mshPageId').value;
					var dataConnectorId = mshPageId + '_' + owner + '_' + refObjId;
					window.DataConnector.updateDataConnector(dataConnectorId, content.ctx);
				}
			}

			services.dcyRenderingEnviromentFactory.setResourcePath(_infos.details.logicalName, _infos.details.resourcePath);

			decisyon.target.content.data 	= content.data;
			decisyon.target.content.ctx 	= content.ctx;

			var resourcePath = _self.getResourcePath();
			decisyon.template.resourcePath = resourcePath;
			// short cut per l'utente per accedere al path delle risorse. L'utente pu� sempre fare override di questo field, tanto la stessa informazione la otterrebbe passando per DECISYON.template.resourcePath.
			templateScope.resourcePath = resourcePath;

			var mainLibraryPath = services.dcyUtilsFactory.getLibraryPath('dcyMainLibrary');
			decisyon.template.mainLibraryPath = mainLibraryPath;
			templateScope.mainLibraryPath = mainLibraryPath;

			templateScope.DECISYON = decisyon;

			templateScope.$applyAsync();

			return templateScope.DECISYON;
		};

		_self.setTemplatePath = function(tmplPath){
			if (services.dcyUtilsFactory.isValidString(tmplPath)) {
				_infos.templatePath = tmplPath;
			}
		};

		_self.getTemplatePath = function(){
			return _infos.templatePath;
		};

		_self.setTemplateName = function(tmplName){
			if (services.dcyUtilsFactory.isValidString(tmplName)) {
				_infos.templateName = tmplName;
			}
		};

		_self.getTemplateName = function(){
			return _infos.templateName;
		};

		_self.setTemplate = function(tmplName, isInternal) {
			var isInternalRenderer = angular.isDefined(isInternal) ? isInternal : true;

			if (!services.dcyUtilsFactory.isValidString(tmplName)) {
				_self.setTemplatePath(getInternalTmplPath(_infos.errorTemplateName));
				_self.setTemplateName(_infos.errorTemplateName);
			}	else {
				if(isInternalRenderer){
					_self.setTemplatePath(getInternalTmplPath(tmplName));
				}else{
					_self.setTemplatePath(templateScope.http != false ? services.dcyUtilsFactory.getTemplatePath(_self.getBasePath()) : tmplName);
				}

				_self.setTemplateName(tmplName);
			}

			templateScope.$applyAsync();

			return true;
		};

		_self.getRefType = function(){
			return _infos.refType;
		};

		_self.getElement = function(){
			return _infos.element;
		};

		_self.getStatus = function(){
			return _infos.status;
		};

		_self.setStatus = function(newStatus){

			switch (newStatus) {

				case _self.statusEnum.STARTED:
					_infos.status = newStatus;
					break;

				case _self.statusEnum.ERROR :
					_infos.status = newStatus;
					break;

				case _self.statusEnum.SUCCESS :
					_infos.status = newStatus;
					break;

				case _self.statusEnum.COMPLETE :
					_infos.status = newStatus;
					break;

				default:
					_infos.status = _self.statusEnum.ERROR;
					break;
			}

			return _infos.status;
		};

		_self.getScope = function(){
			return templateScope;
		};

		_self.setDetails = function(details){
			_infos.details = details;
		};

		_self.getDetails = function(){
			return _infos.details;
		};

		_self.getDataCtx = function(){
			return {
				data : _infos.details.data,
				ctx : _infos.details.ctx
			};
		};

		_self.getBasePath = function(){
			return _infos.details.basePath;
		};

		_self.setLoadingTemplateName = function(tmplName){
			_infos.loadingTemplateName = tmplName;
		};

		_self.getLoadingTemplateName = function(){
			return _infos.loadingTemplateName;
		};

		_self.setErrorTemplateName = function(tmplName){
			_infos.errorTemplateName = tmplName;
		};

		_self.getErrorTemplateName = function(){
			return _infos.errorTemplateName;
		};

		_self.getScope = function(){
			return templateScope;
		};

		_self.update = function(additionalCtx){
			templateScope.$emit('dcy.tmplMng.update', additionalCtx);
		};

		_self.reinit = function(){
			templateScope.$emit('dcy.tmplMng.reinit');
		};
	};
}(window.DSH_PAGE));
