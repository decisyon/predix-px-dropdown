(function() {
	'use strict';
	/**
	 * @ngdoc service
	 * @name dcyApp.dcyCommonFnFactory
	 *
	 * @requires $rootScope
	 * @requires dcyUtilsFactory
	 *
	 * @description
	 * Common functions for all application
	 *
	 *
	 *
	 * @example
	 * ```js
	 * function serviceFn(dcyCommonFnFactory){
	 *    dcyEnvProvider.callFn(params);
	 * }
	 * myFn.$inject = ['dcyCommonFnFactory'];
	 *
	 * angular.module('dcyApp.factories')
	 *     .factory('service', serviceFn);
	 *
	 * ```
	 */
	function CommonFnFactory($rootScope, dcyUtilsFactory, $q, $timeout) {

		/**
		 * @ngdoc method
		 * @name responseValidator
		 * @methodOf dcyApp.dcyCommonFnFactory
		 *
		 * @description
		 * Validator for server response
		 *
		 * @returns {boolean} true if response is valid or false.
		 */
		function responseValidator(response) {

			var isResponseValid = dcyUtilsFactory.isValidObject(response),
			isDataValid = angular.isObject(response.data),
			isViewRenderValid = angular.isDefined(response.view) ? dcyUtilsFactory.isValidString(response.view) : true,
					isCtxValid = angular.isObject(response.ctx);
			return isResponseValid && isDataValid && isViewRenderValid && isCtxValid;
		}
		
		/**
		 * @ngdoc method
		 * @name logout
		 * @methodOf dcyApp.dcyCommonFnFactory
		 *
		 * @description
		 * Logout
		 *
		 * @param {object} $event
		 */
		function logout($event) {
			rootPage.logoutFunction($event);
		}
		
		/**
		 * @ngdoc method
		 * @name getContentMenu
		 * @methodOf dcyApp.dcyCommonFnFactory
		 *
		 * @description
		 * Logout
		 *
		 * @param {string} contentProviderId - obbligatorio
		 * @param {object} ctx - eventuali parametri di contesto
		 */
		function getContentMenu(contentProviderId, ctx) {
			var dcyDPService = dcyUtilsFactory.invokeService('dcyDPService'),
				deferred = $q.defer();
			
			if (!dcyUtilsFactory.isValidString(contentProviderId)) {
				return false;
			}
			
			var requestParams = {
					dpc: contentProviderId
			};
			
			if(dcyUtilsFactory.isValidObject(ctx)){
				requestParams.rasterCtx = window.getRasterCtxContainer(ctx);
			}

			/*
			$timeout(function(){
				deferred.resolve({"contrUID":"templateEditorMenu","contentUID":"GENERIC_DP","useDataLink":false,"view":"groupMenu_BaseTMPL","engineV":"0","viewOwner":"MASTERM","basePath":"/template/T817456321418616T/MASTERM_630350575451332","data":{"groupMenu":{"maxElementReached":false,"minimizable":false,"mnuEntries":[{"desc":"Delete","longDesc":"Delete the folder","action":"DELETE_OBJECTS","visible":true,"enabled":true},{"desc":"Paste","longDesc":"Paste the resource","action":"PASTE_OBJECTS","visible":true,"enabled":true},{"desc":"Cut","longDesc":"Cut the resource","action":"CUT_OBJECTS","visible":true,"enabled":true},{"desc":"Copy","longDesc":"Copy the resource","action":"COPY_TEMPLATES","visible":true,"enabled":true}],"hideMenuButton":false}},"ctx":[{"id":"elementUID","value":"NA_120_321455791872177"},{"id":"menuCtxID","value":"296455791893305"},{"id":"menuCtxType","value":"1"},{"id":"themeTMPL","value":"DEFAULT"},{"id":"instanceReference","value":"889456322034807"},{"id":"contrUID","value":"templateEditorMenu"},{"id":"contentUID","value":"GENERIC_DP"}]});
			});
			 */
			dcyDPService(requestParams)
			.then(
					function(response) {
						deferred.resolve(response);
					},
					function(response) {
						deferred.reject(response);
					}
			);
			return deferred.promise;
		}

		return {
			responseValidator: responseValidator,
			logout: logout,
			getContentMenu : getContentMenu
		};
	}

	CommonFnFactory.$inject = ['$rootScope', 'dcyUtilsFactory', '$q', '$timeout'];
	angular.module('dcyApp.factories').factory('dcyCommonFnFactory', CommonFnFactory);
}());
