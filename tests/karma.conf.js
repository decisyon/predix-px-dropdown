module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns
    basePath: '../',

    // frameworks to use
    frameworks: ['jasmine', 'requirejs'],

    // list of files / patterns to load in the browser
    files: [
      {pattern: 'environment/vendor/jquery/dist/jquery.js', included: true},
      {pattern: 'environment/dcy_api/generic_api.object.js', included: true},
      {pattern: 'environment/commonRootPage.js', included: true},
      {pattern: 'environment/core.js', included: true},
      {pattern: 'environment/environmentVariable.js', included: true},
      {pattern: 'environment/utils.js', included: true},
      {pattern: 'environment/templateCommon.js', included: true},
      {pattern: 'environment/main_informations.js', included: true},

      {pattern: 'environment/**/*.js', included: false , served: true},
      {pattern: 'sources/**/*.js', included: false , served: true },
      {pattern: 'tests/unit/**/*.js', included: false , served: true },

     'tests/test-main.js'      
    ],

    // test result reporter
    reporters: ['progress'],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    logLevel: config.LOG_DEBUG,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    browsers: ['Chrome'],

    // Continuous Integration mode
    singleRun: false
  });
};
