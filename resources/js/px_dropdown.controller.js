(function() {
	'use strict';

	/**
	* Controller to manage the dropdown widget.
	* link: https://www.predix-ui.com/?show=px-dropdown&type=component
	*/
	function PxDropDownCtrl($scope, $timeout, $element, dcyService) {
		var ctrl	                    = this,
			ctx	                        = $scope.DECISYON.target.content.ctx,
			refObjId	                = ctx.refObjId.value,
			target	                    = $scope.DECISYON.target,
			mshPage	                    = target.page,
			baseName	                = ctx.baseName.value,
			selectedIDParamToExport 	= baseName + '_SelectedID',
			selectedDSParamToExport 	= baseName + '_SelectedDS',
			EMPTY_PARAM_VALUE           = 'PARAM_VALUE_NOT_FOUND',
			unBindWatches               = [],
			pxDropdown;

		/*Add translations*/
		var addTranslations = function(){
		    var jsonTranslationsEn = {
   		    	noSelection : 'No selection'
       	    };
       	    var jsonTranslationsIt = {
   		    	noSelection : 'Nessuna selezione'
       	    };

            dcyService.addTranslations(jsonTranslationsEn, 'en');
            dcyService.addTranslations(jsonTranslationsIt, 'it');
		};
		/*Set attribute on pxDropdown element*/
		var setAttributeOnDropdown = function(attrName, attrValue){
		    if(pxDropdown){
		        pxDropdown.setAttribute(attrName, attrValue);
		    }
		};
		/* Functionality of each property*/
		var manageParamsFromContext = function(context) {
		    manageDisabledAttr(context);
		    manageImportedParams(context);
		    manageHideChevronAttr(context);
		};
        /* Management of disabled attribute */
        var manageDisabledAttr = function (context) {
            ctrl.disabled = context.$pxdropdownDisabled.value;
            if (ctrl.disabled){
				setAttributeOnDropdown('disabled', ctrl.disabled);
			}
        };
        /* Management of hideChevron attribute */
        var manageHideChevronAttr = function (context) {
            ctrl.hideChevron = context.$pxdropdownhideChevron.value;
            if (ctrl.hideChevron){
				setAttributeOnDropdown('hide-Chevron', ctrl.hideChevron);
			}
        };
		/* Exporting the param value */
		var sendParams = function(id, value) {
			if (mshPage){
				target.sendParamChanged(selectedIDParamToExport, id);
				target.sendParamChanged(selectedDSParamToExport, value);
			}
		};
        /* Search the id into entries list by callback. If not exist it returns false, otherwise the entire object found. */
        var getEntryFromList = function(filterFn){
           if(!angular.isArray(ctrl.itemList) || !angular.isFunction(filterFn)){
               return false;
           }

           var entry = ctrl.itemList.filter(filterFn);

           return !entry.length ? false : entry[0];
        };
        /* Search the id into entries list by id. */
        var getEntryFromListById = function(idToSearch){
            return getEntryFromList(function(obj){
               return obj.key == idToSearch;
            });
        };
        /* Search the id into entries list by val. */
        var getEntryFromListByDesc = function(descToSearch){
            return getEntryFromList(function(obj){
               return obj.val == descToSearch;
            });
        };
        /* Management of imported parameters */
        var manageImportedParams = function (context) {
		    var selectedIDParamToImport = "_PARAM_"+selectedIDParamToExport,
		        selectedIDFromParams    = angular.isDefined(context[selectedIDParamToImport]) ? context[selectedIDParamToImport].value : '';
		    var entryToSelected         = getEntryFromListById(selectedIDFromParams);

		    if(entryToSelected){
	            setAttributeOnDropdown('display-value', entryToSelected.val);
		    }
        };
         /* Management of display value attribute */
        var setDisplayValuedAttr = function (context) {
            var entry = getEntryFromListByDesc(context.$pxdropdowndisplayValue.value);
            if(entry){
			    setAttributeOnDropdown('display-value', entry.val);
            }
        };
		/* Capture the data of the selected dropdown value */
		var setEventListener = function() {
			if(pxDropdown){
    			pxDropdown.addEventListener('px-dropdown-value-changed',function(e) {
			    	sendParams(e.detail.key, e.detail.val);
			    });
			}
		};
		/* Watch on widget context */
		var listenerOnDataChange = function() {
			unBindWatches.push($scope.$watch('DECISYON.target.content', function(newContent, oldContent) {
				if (!angular.equals(newContent, oldContent)){
					manageParamsFromContext(newContent.ctx);
				}
			}, true));
		};
        var setError = function(errorMsg){
            ctrl.errorMsg   = errorMsg;
            ctrl.currentTpl = 'error.html';
        };
        /* Set data on model */
        var setDropdown = function(context, data){
            if(data){
                ctrl.itemList   = data;
                setDisplayValuedAttr(context);
                ctrl.currentTpl = 'dropdown.html';
            }
            else{
                ctrl.setError();
            }
        };
        /* Update data on model */
        var updateDropdown = function(data){
            ctrl.setDropdown(data);
        };
        /* Manage data for add the no selection entry */
        var manageData = function(data){
            var $pxdropdownShowNoSelection = ctx.$pxdropdownShowNoSelection.value;
            if($pxdropdownShowNoSelection){
                data.unshift({
					'key' : 'PARAM_VALUE_NOT_FOUND',
					'val' : dcyService.getTranslation('noSelection')
                });
            }
            return data;
        };
        /* Get data */
        var getData = function (context) {
            ctrl.useDataConnector = context.useDataConnector.value;
            if (ctrl.useDataConnector == 'true'){
                if (target.getDataConnector) {
		            var dc = target.getDataConnector();
		            dc.ready(function (dataConnector) {
        		        dataConnector.onUpdate(function () {
        		            dataConnector.instance.data.then(function(data){
        		                updateDropdown(context, manageData(data));
        		            }, function(rejection){
        		                setError();
        		            });
        		        });

    		            dataConnector.instance.data.then(function(data){
		                     setDropdown(context, manageData(data));
    		            }, function(rejection){
    		                 setError(rejection.message);
    		            });
        		    });
        		    dc.fail(function (error) {
        		         setError(error.errorMsg);
        		    });
        		}else{
        		    ctrl.itemList = [];
        		}
            }
        };
        /* Destory all listener of widget */
        var destroyWidgets = function(){
            for(var unBind in unBindWatches){
                unBind();
            }
        };
        /* Initialize */
        var initialize = function(){
            ctrl.widgetID	 = 'pxdropdown_' + refObjId;
            addTranslations();
		    getData(ctx);
    	};

        unBindWatches.push($scope.$on('$includeContentRequested', function(angularEvent, src){
            if(src == 'dropdown.html'){
                $timeout(function(){
                    pxDropdown = document.getElementById(ctrl.widgetID);
                    manageParamsFromContext(ctx);
        			listenerOnDataChange();
    	    		setEventListener();
                });
            }
            else{
                destroyWidgets();
            }
        }));

		initialize();
	}

	PxDropDownCtrl.$inject = ['$scope', '$timeout', '$element', 'dcyService'];
	DECISYON.ng.register.controller('pxDropDownPredixCtrl', PxDropDownCtrl);
}());